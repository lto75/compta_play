# --- First database schema

# --- !Ups

set ignorecase true;

create table client (
  id                        bigint not null,
  name                      varchar(255) not null,
  siret                      varchar(255),
  address                      varchar(255),
  constraint pk_client primary key (id))
;

create table facture (
  id                        bigint not null,
  name                      varchar(255) not null,
  number                    varchar(10) not null,
  payment                   timestamp,
  introduced                timestamp,
  discontinued              timestamp,
  client_id                 bigint,
  statut_id                 bigint,
  constraint pk_facture primary key (id))
;

create table statut (
  id                        bigint not null,
  name                      varchar(255) not null,
  constraint pk_statut primary key (id))
;

create sequence client_seq start with 1000;

create sequence facture_seq start with 1000;

create sequence statut_seq start with 1000;

alter table facture add constraint fk_facture_client_1 foreign key (client_id) references client (id) on delete restrict on update restrict;
create index ix_facture_client_1 on facture (client_id);

alter table facture add constraint fk_facture_statut_1 foreign key (statut_id) references statut (id) on delete restrict on update restrict;
create index ix_facture_statut_1 on facture (statut_id);


# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists client;
drop table if exists facture;
drop table if exists statut;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists client_seq;
drop sequence if exists facture_seq;
drop sequence if exists statut_seq;

