# --- Sample dataset

# --- !Ups

insert into client (id,name,siret,address) values (1,'Apple Inc.', 12346578910, '12 rue des cons 93250 Montreuil');
insert into client (id,name,siret,address) values (2,'Thinking Machines', 12346578910, '13 rue des cons 93250 Montreuil');
insert into client (id,name,siret,address) values (3,'RCA', 12346578910, '14 rue des cons 93250 Montreuil');
insert into client (id,name,siret,address) values (4,'Netronics', 12346578910, '15 rue des cons 93250 Montreuil');
insert into client (id,name,siret,address) values (5,'Tandy Corporation', 12346578910, '16 rue des cons 93250 Montreuil');
insert into client (id,name,siret,address) values (6,'Commodore International', 12346578910, '17 rue des cons 93250 Montreuil');
insert into client (id,name,siret,address) values (7,'MOS Technology', 12346578910, '18 rue des cons 93250 Montreuil');

insert into statut (id,name) values (1,'Generated');
insert into statut (id,name) values (2,'Sent');
insert into statut (id,name) values (3,'Payment');
insert into statut (id,name) values (4,'Payed');

insert into facture (id,name,introduced,discontinued,client_id,number,payment,statut_id) values (1,'MacBook Pro 15.4 inch','1999-05-22','1999-05-22',1,100023,'1999-05-22',2);
insert into facture (id,name,introduced,discontinued,client_id,number,payment,statut_id) values (2,'CM-2a',null,null,2,11123,'1999-05-22',1);
insert into facture (id,name,introduced,discontinued,client_id,number,payment,statut_id) values (3,'CM-200',null,null,2,12223,'1999-05-22',3);
insert into facture (id,name,introduced,discontinued,client_id,number,payment,statut_id) values (4,'CM-5e',null,null,2,133323,'1999-05-22',2);


# --- !Downs

delete from facture;
delete from client;
delete from statut;