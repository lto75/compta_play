package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._

import anorm._

import views._
import models._

/**
 * Manage a database of clients
 */
object Clients extends Controller {

  /**
   * This result directly redirect to the application home.
   */
  val Home = Redirect(routes.Clients.create())

  /**
   * Describe the client form (used in both edit and create screens).
   */
  val clientForm = Form(
    mapping(
      "id" -> ignored(NotAssigned:Pk[Long]),
      "name" -> nonEmptyText,
      "siret" ->nonEmptyText,
      "address" -> nonEmptyText
    )(Client.apply)(Client.unapply)
  )

  // -- Actions


  /**
   * Handle default path requests, redirect to clients list
   */
  def index = Action { Home }
/*
  /**
   * Display the paginated list of clients.
   *
   * @param page Current page number (starts from 0)
   * @param orderBy Column to be sorted
   * @param filter Filter applied on clients names
   */
  def list(page: Int, orderBy: Int, filter: String) = Action { implicit request =>
    Ok(html.clients.list(
      Client.list(page = page, orderBy = orderBy, filter = ("%"+filter+"%")),
      orderBy, filter
    ))
  }
*/
  /**
   * Display the 'edit form' of a existing Client.
   *
   * @param id Id of the client to edit
   */
  def edit(id: Long) = Action {
    Client.findById(id).map { client =>
      Ok(html.clients.editForm(id, clientForm.fill(client)))
    }.getOrElse(NotFound)
  }

  /**
   * Handle the 'edit form' submission
   *
   * @param id Id of the client to edit
   */
  def update(id: Long) = Action { implicit request =>
    clientForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.clients.editForm(id, formWithErrors)),
      client => {
        Client.update(id, client)
        Home.flashing("success" -> "Client %s has been updated".format(client.name))
      }
    )
  }

  /**
   * Display the 'new client form'.
   */
  def create = Action {
    Ok(html.clients.createForm(clientForm))
  }

  /**
   * Handle the 'new client form' submission.
   */
  def save = Action { implicit request =>
    clientForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.clients.createForm(formWithErrors)),
      client => {
        Client.insert(client)
        Home.flashing("success" -> "Client %s has been created".format(client.name))
      }
    )
  }

  /**
   * Handle client deletion.
   */
  def delete(id: Long) = Action {
    Client.delete(id)
    Home.flashing("success" -> "Client has been deleted")
  }

}
