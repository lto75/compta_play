package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._

import anorm._

import views._
import models._

/**
 * Manage a database of factures
 */
object Factures extends Controller {

  /**
   * This result directly redirect to the application home.
   */
  val Home = Redirect(routes.Factures.list(0, 2, ""))

  /**
   * Describe the facture form (used in both edit and create screens).
   */
  val factureForm = Form(
    mapping(
      "id" -> ignored(NotAssigned:Pk[Long]),
      "name" -> nonEmptyText,
      "payment" -> optional(date("dd/MM/yyyy")),
      "number" -> nonEmptyText,
      "introduced" -> optional(date("dd/MM/yyyy")),
      "discontinued" -> optional(date("dd/MM/yyyy")),
      "client" -> optional(longNumber),
      "statut" -> optional(longNumber)
    )(Facture.apply)(Facture.unapply)
  )

  // -- Actions


  /**
   * Handle default path requests, redirect to factures list
   */
  def index = Action { Home }

  /**
   * Display the paginated list of factures.
   *
   * @param page Current page number (starts from 0)
   * @param orderBy Column to be sorted
   * @param filter Filter applied on facture names
   */
  def list(page: Int, orderBy: Int, filter: String) = Action { implicit request =>
    Ok(html.factures.list(
      Facture.list(page = page, orderBy = orderBy, filter = ("%"+filter+"%")),
      orderBy, filter
    ))
  }

  /**
   * Display the 'edit form' of a existing Facture.
   *
   * @param id Id of the facture to edit
   */
  def edit(id: Long) = Action {
    Facture.findById(id).map { facture =>
      Ok(html.factures.editForm(id, factureForm.fill(facture)))
    }.getOrElse(NotFound)
  }

  /**
   * Handle the 'edit form' submission
   *
   * @param id Id of the facture to edit
   */
  def update(id: Long) = Action { implicit request =>
    factureForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.factures.editForm(id, formWithErrors)),
      facture => {
        Facture.update(id, facture)
        Home.flashing("success" -> "Facture %s has been updated".format(facture.name))
      }
    )
  }

  /**
   * Display the 'new facture form'.
   */
  def create = Action {
    Ok(html.factures.createForm(factureForm))
  }

  /**
   * Handle the 'new facture form' submission.
   */
  def save = Action { implicit request =>
    factureForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.factures.createForm(formWithErrors)),
      facture => {
        Facture.insert(facture)
        Home.flashing("success" -> "Facture %s has been created".format(facture.name))
      }
    )
  }

  /**
   * Handle facture deletion.
   */
  def delete(id: Long) = Action {
    Facture.delete(id)
    Home.flashing("success" -> "Facture has been deleted")
  }

}
