package models

import play.api.db._
import play.api.Play.current
import anorm._
import anorm.SqlParser._

case class Statut(id: Pk[Long] = NotAssigned, name: String)

object Statut {

  /**
   * Parse a Client from a ResultSet
   */
  val simple = {
    get[Pk[Long]]("statut.id") ~
    get[String]("statut.name") map {
      case id~name => Statut(id, name)
    }
  }

  /**
   * Construct the Map[String,String] needed to fill a select options set.
   */
  def options: Seq[(String,String)] = DB.withConnection { implicit connection =>
    SQL("select * from statut order by name").as(Statut.simple *).map(c => c.id.toString -> c.name)
  }

}
