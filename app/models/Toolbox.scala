package models

class Toolbox {


  /*
   * Convert a string to int
   */
  def toHex(s: String): Long = {
    val Hex = "([0-9a-fA-F]+)".r
    s match {
      case Hex(_) => java.lang.Long.parseLong(s, 16)
      case _ => throw new NumberFormatException("invalid hex number: " + s)
    }
  }






}