package models

import java.util.{Date}
import play.api.db._
import play.api.Play.current
import anorm._
import anorm.SqlParser._
import java.util.Calendar


case class Facture(id: Pk[Long] = NotAssigned, name: String, payment: Option[Date], number : String, introduced: Option[Date], discontinued: Option[Date], clientId: Option[Long], statutId: Option[Long])

/**
 * Helper for pagination.
 */
case class Page[A](items: Seq[A], page: Int, offset: Long, total: Long) {
  lazy val prev = Option(page - 1).filter(_ >= 0)
  lazy val next = Option(page + 1).filter(_ => (offset + items.size) < total)
}

object Facture {

  // -- Parsers

  /**
   * Parse a Facture from a ResultSet
   */
  val simple = {
    get[Pk[Long]]("facture.id") ~
    get[String]("facture.name") ~
    get[Option[Date]]("facture.payment") ~
    get[String]("facture.number") ~
    get[Option[Date]]("facture.introduced") ~
    get[Option[Date]]("facture.discontinued") ~
    get[Option[Long]]("facture.client_id") ~
    get[Option[Long]]("facture.statut_id") map {
      case id~name~payment~number~introduced~discontinued~clientId~statutId => Facture(id, name, payment, number, introduced, discontinued, clientId, statutId)
    }
  }

  /**
   * Parse a (Facture,Client) from a ResultSet
   */
  val withClient = Facture.simple ~ (Client.simple ?) map {
    case facture~client => (facture,client)
  }

    /**
   * Parse a (Facture,Statut) from a ResultSet
   */
  val withStatut = Statut.simple ~ (Statut.simple ?) map {
    case facture~statut => (facture,statut)
  }


  // -- Queries

  /**
   * Retrieve a facture from the id.
   */
  def findById(id: Long): Option[Facture] = {
    DB.withConnection { implicit connection =>
      SQL("select * from facture where id = {id}").on('id -> id).as(Facture.simple.singleOpt)
    }
  }

  /**
   * Return a page of (Facture,Client).
   *
   * @param page Page to display
   * @param pageSize Number of factures per page
   * @param orderBy Facture property used for sorting
   * @param filter Filter applied on the name column
   */
  def list(page: Int = 0, pageSize: Int = 10, orderBy: Int = 1, filter: String = "%"): Page[(Facture, Option[Client])] = {

    val offest = pageSize * page

    DB.withConnection { implicit connection =>

      val factures = SQL(
        """
          select * from facture
          left join client on facture.client_id = client.id
          where facture.name like {filter}
          order by {orderBy} nulls last
          limit {pageSize} offset {offset}
        """
      ).on(
        'pageSize -> pageSize,
        'offset -> offest,
        'filter -> filter,
        'orderBy -> orderBy
      ).as(Facture.withClient *)

      val totalRows = SQL(
        """
          select count(*) from facture
          left join client on facture.client_id = client.id
          where facture.name like {filter}
        """
      ).on(
        'filter -> filter
      ).as(scalar[Long].single)

      Page(factures, page, offest, totalRows)

    }

  }

  /**
   * Update a facture.
   *
   * @param id The facture id
   * @param facture The facture values.
   */
  def update(id: Long, facture: Facture) = {
    DB.withConnection { implicit connection =>
      SQL(
        """
          UPDATE facture
          SET name = {name},
          payment = {payment},
          introduced = {introduced},
          discontinued = {discontinued},
          client_id = {client_id},
          statut_id = {statut_id}
          WHERE id = {id}
        """
      ).on(
        'id -> id,
        'name -> facture.name,
        'payment -> facture.payment,
        'introduced -> facture.introduced,
        'discontinued -> facture.discontinued,
        'client_id -> facture.clientId,
        'statut_id -> facture.statutId
      ).executeUpdate()
    }
  }

  /**
   * Insert a new facture.
   *
   * @param facture The facture values.
   */
  def insert(facture: Facture) = {
    // Numero de facture
    DB.withConnection { implicit connection =>
      SQL(
        """
          INSERT INTO facture VALUES (
            (SELECT next value for facture_seq),
            {name}, {number}, {payment}, {introduced}, {discontinued}, {client_id}, {statut_id}
          )
        """
      ).on(
        'name -> facture.name,
        'number -> 11111,//generateNumber(),
        'payment -> facture.payment,
        'introduced -> facture.introduced,
        'discontinued -> facture.discontinued,
        'client_id -> facture.clientId,
        'statut_id -> facture.statutId
      ).executeUpdate()
    }
  }

  /**
   * Delete a facture.
   *
   * @param id Id of the facture to delete.
   */
  def delete(id: Long) = {
    DB.withConnection { implicit connection =>
      SQL("delete from facture where id = {id}").on('id -> id).executeUpdate()
    }
  }

/*  def generateNumber(): Long = {
    DB.withConnection { implicit connection =>
      val max:Long = SQL("SELECT MAX(number) from facture").as(scalar[Long].single)

      val number = (new java.text.SimpleDateFormat("yyyyMM")).toString()
      val max1 = java.lang.Long.parseLong(number)
       //number == max.toString() match {
         //case number => (max+1).toString()
         //case _ => number + "0001"
      //}
      max1

    }
  }
*/

}

