package models

import play.api.db._
import play.api.Play.current
import anorm._
import anorm.SqlParser._

case class Client(id: Pk[Long] = NotAssigned, name: String, siret: String, address: String)

object Client {

  /**
   * Parser
   */
  val simple = {
    get[Pk[Long]]("client.id") ~
    get[String]("client.name") ~
    get[String]("client.siret") ~
    get[String]("client.address") map {
      case id~name~siret~address => Client(id, name, siret, address)
    }
  }

  /**
   * Construct the Map[String,String] needed to fill a select options set.
   */
  def options: Seq[(String,String)] = DB.withConnection { implicit connection =>
    SQL("select * from client order by name").as(Client.simple *).map(c => c.id.toString -> c.name)
  }


  // -- Queries

  /**
   * Retrieve a client from the id.
   */
  def findById(id: Long): Option[Client] = {
    DB.withConnection { implicit connection =>
      SQL("select * from client where id = {id}").on('id -> id).as(Client.simple.singleOpt)
    }
  }

    /**
   * Update a client.
   *
   * @param id The client id
   * @param client The client values.
   */
  def update(id: Long, client: Client) = {
    DB.withConnection { implicit connection =>
      SQL(
        """
          UPDATE client
          SET name = {name},
          siret = {siret},
          address = {address}
          WHERE id = {id}
        """
      ).on(
        'id -> id,
        'name -> client.name,
        'siret -> client.siret,
        'address -> client.address
      ).executeUpdate()
    }
  }

  /**
   * Insert a new client.
   *
   * @param client The client values.
   */
  def insert(client: Client) = {
    // Numero de client
    DB.withConnection { implicit connection =>
      SQL(
        """
          INSERT INTO client VALUES (
            (SELECT next value for client_seq),
            {name}, {siret}, {address}
          )
        """
      ).on(
        'name -> client.name,
        'siret -> client.siret,
        'address -> client.address
      ).executeUpdate()
    }
  }

    /**
   * Delete a client.
   *
   * @param id Id of the client to delete.
   */
  def delete(id: Long) = {
    DB.withConnection { implicit connection =>
      SQL("delete from client where id = {id}").on('id -> id).executeUpdate()
    }
  }




}